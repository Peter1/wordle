from wordle import *
import words as wds
import random
import copy

def filter_word_list(words, clues):
  """
  Gives a list of possible words based on previous guesses and clues.
  
  Accepts the list of words, and the previous clues. Returns a list of
  words that could possibly be the secret word
  """
  if len(clues) == 0:
    return wds.words
  
  # Set to avoid duplicates
  possible_words = set()

  for word in words:
    word = word.upper() # Uppercase??

    # To use later
    matches_previous_clues = True

    for guess, clue in clues:
      generated_clue = check_word(word, guess)
      if generated_clue != clue:
        # DO NOT add to the possible_words set
        matches_previous_clues = False
        break # breaks out of lower loop because no reason to keep checking if one fails

    if matches_previous_clues:
      possible_words.add(word.lower())
    
        
  return list(possible_words)

def easy_game(secret):
  """
  Implements the game. Takes a string that is the secret word. Does not
  return a value
  """
  clues = []
  print_stuff = False # Don't print on first run
  i = 0
  possible_words = copy.deepcopy(wds.words)
  
  while i < 6:
    # Check if game should print possible values
    if print_stuff:
      possible_words = filter_word_list(possible_words, clues)
      print(f"{len(possible_words)} words possible:")
      if len(possible_words) > 5:
        to_print = random.sample(possible_words, 5)
        print(*to_print, sep="\n")
      else:
        print(*possible_words, sep="\n")
        
    print_stuff = True # Default should print game information
    guess = input("> ")
    current_clue = check_word(secret, guess)

    if current_clue == -1: # Check if word is valid
      print("Not a word. Try again")
      print_stuff = False
    elif current_clue == [ "green", "green", "green", "green", "green" ]: # Check for correct answer
      clues.append((guess, current_clue))
      print_all_guess(clues)
      break
    else: # Is a valid word, but is not the secret word
      i += 1 # Increment iterator variable
      clues.append((guess, current_clue))
      print_all_guess(clues)

  # Need to print in this format for the autograder
  possible_words = filter_word_list(possible_words, clues)
  print(f"{len(possible_words)} words possible:")
  if len(possible_words) > 5:
    to_print = random.sample(possible_words, 5)
    print(*to_print, sep="\n")
  else:
    print(*possible_words, sep="\n")

  print(f"Answer: {secret.upper()}")
  
def main():
  """
  Main function
  """
  secret = random.choice(wds.words)
  easy_game(secret)

if __name__ == "__main__":
  main()

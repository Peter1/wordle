import words
import random
import display_utility as du

def check_word(secret, guess):
  """
  Checks a word that is inputed as a guess and generates the clue.

  Secret input is a string representation of the secret word.

  Guess is a string representation of the user's guess.

  Returns a list of length five containing strings "Green", "Yellow"
  or "Grey" where the color corrisponds with the associated index of
  guess.
  """

  secret = secret.lower()
  guess = guess.lower()

  # quick check to see if the guess is correct. This is done
  # because it is easy to check and there is no reason to go through
  # all the below steps if they are equal
  if secret == guess:
    return [ "green", "green", "green", "green", "green" ]

  # Check that guess is in the words list
  if guess not in words.words:
    return -1

  # Start with a list of all grey letters
  result = [ "grey", "grey", "grey", "grey", "grey" ]
  # Easier to handle these in a list then a string
  secret = list(secret)
  guess  = list(guess)

  # Get greens first
  for i in range(len(guess)):
    if secret[i] == guess[i]:
      result[i] = "green"
      # Mark as used (to avoid double letter problems)
      secret[secret.index(guess[i])] = "0"
      guess[i] = "-" # Mark as used

  # Now get yellows
  for i in range(len(guess)):
    if guess[i] in secret:
      result[i] = "yellow"
      secret[secret.index(guess[i])] = "0" # Mark as used
      guess[i] = "-" # Mark as used
      
  return result

def known_word(clues):
  """
  Outputs the known position of each letter. For example, __ING where
  positions which have not seen a green clue are presented as _ and
  positions which have received a green clue are the known letter.
  
  Accepts a list of tuples that represents the hints that were given
  to the guesser previously.
  
  Returns the known position of each letter.
  """
  # Default no letters known
  letters = list("_____")

  # If there is no clues yet then return the default of no known letters
  if len(clues) == 0:
    return "_____"

  for word, colors in clues:
    for i in range(len(colors)):
      if colors[i] == "green":
        letters[i] = word[i].upper()
        
  # list => string
  return "".join(letters)
  

def no_letters(clues):
  """
  Gets the letters that are known NOT to be in the list, in
  alphabetical order and all caps. If there is a double letter, then
  the word is not included in the list, even if the letter is guessed
  in a situation it would normally show as grey.
  
  Accepts a list of tuples that represents the hints that were given
  to the guesser previously. Returns a string of letters we know are
  not in the word, in alphabetical order.
  """
  # Use a set to avoid duplicates
  letters = {}
  letters_not_in_word = []

  for word, colors in clues:
    for i in range(len(colors)):
      if word[i] in letters:
        continue
      letters[word[i]] = colors[i]

  for letter in letters.keys():
    if letters[letter] == "grey":
      letters_not_in_word.append(letter)
      
  return "".join(sorted(letters_not_in_word))


def yes_letters(clues):
  """
  The opposite of no_letters. Returns the letters that are known to be
  in the list.

  Accepts a list of tuples that represents the hints that were given.
  Returns a string of letters that are in the word, in alphabetical
  order.
  """
  letters_in_word = set()

  for word, colors in clues:
    for i in range(len(colors)):
      if colors[i] == "green" or colors[i] == "yellow":
        letters_in_word.add(word[i])

  return "".join(sorted(letters_in_word))

def print_all_guess(clues):
  """
  Prints all the letters in the clues with the correct color.

  Accepts a clues list as input. Returns None.
  """
  for word, clue in clues:
    word = word.upper()
    for i in range(len(word)):
      if clue[i] == "green":
        du.green(word[i])
      elif clue[i] == "yellow":
        du.yellow(word[i])
      else:
        du.grey(word[i])
    print() # New Line

def game(secret):
  """
  Implements the game. Takes a string that is the secret word.
  """
  clues = []
  print_stuff = True
  i = 0
  
  while i < 6:
    if print_stuff:
      print("Known:", known_word(clues))
      print("Green/Yellow Letters:", yes_letters(clues).upper())
      print("Grey Letters:", no_letters(clues).upper())
    
    print_stuff = True
    guess = input("> ")
    current_clue = check_word(secret, guess)
      
    if current_clue == -1:
      print("Not a word. Try again")
      print_stuff = False
    elif current_clue == [ "green", "green", "green", "green", "green" ]:
      clues.append((guess, current_clue))
      print_all_guess(clues)
      break
    else:
      i += 1
      clues.append((guess, current_clue))
      print_all_guess(clues)

  # print_all_guess(clues)
  print("Answer:", secret.upper())

def main():
  secret = random.choice(words.words)
  game(secret)

if __name__ == "__main__":
  main()
